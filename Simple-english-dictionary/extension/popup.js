

document.getElementById('searchButton').addEventListener('click', function() {
    let isOnline = navigator.onLine;
    if (isOnline) {

        document.getElementById('searchButton').disabled = true;
        let wordToSearch = document.getElementById('wordInput').value; 
        
        if (wordToSearch != '') {

            fetch(`https://api.dictionaryapi.dev/api/v2/entries/en/${wordToSearch}`)
            .then((response) => response.json())
            .then((data) => {
                if (data.title == 'No Definitions Found') {
                    document.getElementById('searchButton').disabled = false;  
                    document.getElementById('wordData').innerHTML = `
                        <p>No definitions found</p>
                    `;
                } else {
                    let wordMeaning = data[0].meanings[0].definitions[0].definition;
                    let meanings = data[0].meanings;
                    let defItems = '';
                    for (let i=0; i<meanings.length; i++) {
                        defItems += `
                            <p>
                                <strong>${i+1}. ${meanings[i].partOfSpeech}</strong>
                                <br>
                                ${meanings[i].definitions[0].definition}      
                            </p>            
                        `;
                    };
                    document.getElementById('wordData').innerHTML = defItems;     
                    document.getElementById('searchButton').disabled = false;     
                }
            });

        } else {
            document.getElementById('searchButton').disabled = false;
        }
    } else {
        document.getElementById('wordData').innerHTML = `
            <p style="color: red;">No internet conection</p>
        `;
    }
});

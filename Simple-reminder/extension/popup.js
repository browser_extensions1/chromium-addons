
// event when click on save reminder button
document.getElementById('submitReminderButton').addEventListener('click', function() {
    let reminderString = document.getElementById('textareaInput').value; 
    let minutesValue = document.getElementById('minutesInput').value;
    let hoursValue = document.getElementById('hoursInput').value;
    let minutesDelay = Math.round(hoursValue*60 + minutesValue);
    if (reminderString !== '' && reminderString.length<=60) {
        if (minutesValue>0 || hoursValue>0) {
            // creates and save alarm
            saveReminder(reminderString, minutesDelay);
            renderListHtml();
        };
    };
    
});


// save to storage 
function saveReminder(reminderString, totalMinutes) {
    chrome.storage.local.get(['reminderList'], function(result) {
        if (result.reminderList) {
            // updates the array
            let reminderArray = result.reminderList; 
            let lastIndex = reminderArray.length;
            reminderArray.push({
                id: lastIndex,
                isNotified: false,
                reminderText: reminderString 
            });
            
            // saves the new reminder to storage 
            chrome.storage.local.set({reminderList: reminderArray}, function() {
                
                // creates the alarm
                chrome.alarms.create(
                    `reminder-${lastIndex}`,
                    {delayInMinutes: totalMinutes}
                    );
                })
        } else {
                // creates the array
                let reminderArray = [];
            reminderArray.push({
                id: 0, 
                isNotified: false,
                reminderText: reminderString
            });

            // saves to storage 
            chrome.storage.local.set({reminderList: reminderArray}, function() {
                // creates the alarm 
                chrome.alarms.create(
                    `reminder-0`,
                    {delayInMinutes: totalMinutes}
                )
            });

        }
    })
};


function renderListHtml() {
    chrome.storage.local.get(['reminderList'], function(result) {
        if (result.reminderList) {
            let reminderArray = result.reminderList; 
            let ulHtml = '';
            for (let i=0; i<reminderArray.length; i++) {
                let reminderPart = reminderArray[i].reminderText.substr(0,30);
                let reminderStyle = '';
                if (reminderArray[i].isNotified) {
                    reminderStyle = 'style="color: gray; font-size: 0.8em;"';
                } else {
                    reminderStyle = 'style="color: black; font-size: 0.8em;"';
                };
                ulHtml += `
                    <li ${reminderStyle}>${reminderPart} ...</li>
                `;
            };
            document.getElementById('ulList').innerHTML = ulHtml;
        }
    })
};


document.getElementById('clearAllbutton').addEventListener('click', function() {
    chrome.alarms.clearAll(function() {
        chrome.storage.local.set({reminderList: []}, function() {
            renderListHtml();
        });
    })
});

renderListHtml();


chrome.runtime.onInstalled.addListener( (event) => {
    if (event.reason === chrome.runtime.OnInstalledReason.INSTALL) {
        chrome.tabs.create(
            {url: "https://vwrr-extensions.click"}
        )
        console.log(">>> Add-on installed successfully")
    } else if (event.reason === chrome.runtime.OnInstalledReason.UPDATE) {
        console.log(">>> Add-on updated successfully")
    }
})



chrome.alarms.onAlarm.addListener(function( alarm ) {

    let alarmIndex = alarm.name.split('-')[1];

    // gets the alarm's data 
    chrome.storage.local.get(['reminderList'], function(result) {
        let reminderArray = result.reminderList; 
        let alarmItem = reminderArray[alarmIndex];

        // creates notification
        chrome.notifications.create(
            `notification-${alarmIndex}`,
            {
                type: "basic",
                iconUrl: "icons/logo_48x48.png",
                title: "Reminder notification",
                message: alarmItem.reminderText,
                eventTime: Date.now() + 2000
            },
            function() {
                // once notified update reminders list and save
                reminderArray[alarmIndex].isNotified = true;
                chrome.storage.local.set({reminderList: reminderArray}, function() {
                    console.log('> remind')
                });
            }
        )
    })
});




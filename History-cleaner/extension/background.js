const data = {
  "appcache": {name: "Websites' appcaches", checked: true},
  "cache": {name: "Browser's cache", checked: true},
  "cacheStorage": {name: "Cache storage ", checked: true},
  "cookies": {name: "Browser's cookies", checked: true},
  "downloads": {name: "Browser's download list ", checked: true},
  "fileSystems": {name: "Websites' file systems", checked: true},
  "formData": {name: "Browser's stored form data", checked: true},
  "history": {name: "Browser's history", checked: true},
  "indexedDB": {name: "Websites' IndexedDB data", checked: true},
  "localStorage": {name: "Websites' local storage data", checked: true},
  "passwords": {name: "Stored passwords", checked: true},
  "serviceWorkers": {name: "Service Workers", checked: true},
  "webSQL": {name: "Websites' WebSQL data", checked: true},
};
const initialData = JSON.stringify(data);

chrome.runtime.onInstalled.addListener( (event) => {
  
  chrome.storage.local.set({"options": initialData}, () => {
    if (event.reason === chrome.runtime.OnInstalledReason.INSTALL) {
      chrome.tabs.create({
        url: 'https://vwrr-extensions.click/'
      });
      console.log(">>> add-on installed successfully")
    } else if (event.reason === chrome.runtime.OnInstalledReason.UPDATE) {
      console.log(">>> add-on updated successfully")
    }
  });


});
 

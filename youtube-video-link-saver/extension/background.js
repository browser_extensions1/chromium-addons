const initialData = JSON.stringify([])

chrome.runtime.onInstalled.addListener( (event) => {
  
  if (event.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.storage.local.get(["videos"], (result) => {
      if (!result.videos) {
        chrome.storage.local.set({"videos": initialData}, () => {
          console.log('>>> Add-on installed successfully');
        });
      };
    });

    chrome.tabs.create({
      url: 'https://vwrr-extensions.click/'
    });

  } else if (event.reason === chrome.runtime.OnInstalledReason.UPDATE) {
    
    chrome.storage.local.get(["linkList"], (result) => {
      if (result.linkList) {
        let initData = JSON.stringify(result.linkList);
        chrome.storage.local.set({"videos": initData}, () => {
          console.log('>>> Add-on installed successfully');
        })
      } else {
        chrome.storage.local.get(["videos"], (result) => {
          if (!result.videos) {
            chrome.storage.local.set({"videos": initialData}, () => {
              console.log('>>> Add-on installed successfully');
            });
          };
        });
      }
    });

    chrome.tabs.create({
      url: 'https://vwrr-extensions.click/'
    });

  } else {
    chrome.storage.local.get(["videos"], (result) => {
      if (!result.videos) {
        chrome.storage.local.set({"videos": initialData}, () => {
          console.log('>>> Add-on installed successfully');
        });
      };
    });

    chrome.tabs.create({
      url: 'https://vwrr-extensions.click/'
    });

  };

  console.log(event.reason);

})
  

var currentDate = new Date();


// creates the month name and year name
var stringMonth = new Intl.DateTimeFormat('en-US', {month: 'long'}).format(currentDate);
document.getElementById('currentMonth').innerHTML = `${stringMonth} - ${currentDate.getFullYear()}`;


function getTotalMonthDays(year, month) {
    let ddate = new Date(year, month + 1, 0);
    let totalMonthDays = ddate.getDate();
    return totalMonthDays;
};


function getWeekdayMonthday(year, month, monthday) {
    var ddate = new Date(year, month, monthday);
    let weekday = ddate.getDay();
    return weekday; // 0 to 6 , sunday to saturday
};


// creates first week 
var weekone = [];
var firstweekday = getWeekdayMonthday(
    currentDate.getFullYear(),
    currentDate.getMonth(),
    1
);
for (let i=0; i<7; i++) {
    if( i>=firstweekday ){
        weekone.push(String(i+1-firstweekday))
    } else {
        weekone.push('');
    };
};


// make cuts for every week
var monthTotalDays = getTotalMonthDays(
    currentDate.getFullYear(),
    currentDate.getMonth()
);
var cutone = 8 - firstweekday;
var cuts = [cutone];
for (let p=0; p<6; p++) {
    cutone = cutone + 7;
    if (cutone <= monthTotalDays) {
        cuts.push(cutone);

    };
};


// create the weeks as arrays
var weeks = {0: weekone}
for (let t=0; t<cuts.length; t++) {
    let week = [];
    for (let m=0; m<7; m++) {
        if (m+cuts[t]>monthTotalDays) {
            week.push("")
        } else {
            week.push(String(m+cuts[t]))
        }
    };
    weeks[t+1] = week;
};


// add the weeks to the table
var weekrows = '';
var weekslenght = Object.keys(weeks).length;
for (let h=0; h<weekslenght; h++) {
    var roww = '';
    for (let v=0; v<weeks[h].length; v++) {
        roww += `<td id="${weeks[h][v]}">${weeks[h][v]}</td>`;
    };
    weekrows += `<tr>${roww}</tr>`;
};

// insert the calendar to the dom
document.getElementById('tbody').innerHTML = weekrows


// show the current date 
var currentDayMonth = currentDate.getDate();
document.getElementById(`${currentDayMonth}`).style.backgroundColor = '#fc9197';


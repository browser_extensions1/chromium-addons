var data = [
  {name: 'Create note', content: 'To create a note, push the button "New note".', color: '#a48ce4'},
  {name: 'Delete note', content: 'To delete a note, push the red button.', color: '#e48c8c'},
  {name: 'See note', content: 'Click on the title note to see the entire content.', color: '#f7df7c'},
  {name: 'Edit note', content: 'Edit note. Click on the title note, then click the "Edit" button to change the title note and content.', color: '#b6d7a8'}
];
var initialData = JSON.stringify(data);


chrome.runtime.onInstalled.addListener( (result) => {
  if (result.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.storage.local.get(["notes"], (result) => {
      if (!result.notes) {
        chrome.storage.local.set({"notes": initialData}, () => {
          console.log('>>> initial data set');
        });
      };
    });
    chrome.tabs.create({
      url: 'https://vwrr-extensions.click/'
    });
  }
})

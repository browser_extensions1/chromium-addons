

generateVerse();


document.getElementById("generateVerseButton").addEventListener('click', function(){
    generateVerse();    
});


function generateVerse() {
    let newVerse = '';
    let randomNumber = Math.round(Math.random()*2461)
    document.getElementById('verseText').innerHTML = psalmsVerses[randomNumber].text; 
    document.getElementById('verseId').innerHTML = `
        Psalms ${psalmsVerses[randomNumber].chapter}: ${psalmsVerses[randomNumber].verse}
    `; 
};


document.getElementById('switchModeButton').addEventListener('click', function() {
    chrome.storage.local.get(['darkMode'], function(result) {
        const bodyClass = document.body.classList;
        if (result.darkMode) {
            chrome.storage.local.set({darkMode: false}, function() {
                bodyClass.remove('dark');
            })
        } else {
            chrome.storage.local.set({darkMode: true}, function() {
                bodyClass.add('dark');
            })
        };
    })
});


chrome.storage.local.get(['darkMode'], function(result) {
    if (result.darkMode) {
        const bodyClass = document.body.classList;
        result.darkMode
            ? bodyClass.add('dark') 
            : bodyClass.remove('dark');
    }
})


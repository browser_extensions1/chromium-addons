let isOnline = navigator.onLine;

if (isOnline) {

    var map = L.map('map');

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
    }).addTo(map);

    chrome.storage.local.get(['mapsettings'], function(result) {

        if (result.mapsettings) {
            let longitude = result.mapsettings.longitude;
            let latitude = result.mapsettings.latitude;
            let zoom = result.mapsettings.zoom;
            map.setView([latitude, longitude], zoom);
        } else {
            map.setView([51.505, -0.09], 3);
        }

    });

    map.on('mouseup', function(e) {
        let mapsettings = {
            longitude: map.getCenter().lng, 
            latitude: map.getCenter().lat,
            zoom: map.getZoom()
        };

        chrome.storage.local.set({mapsettings: mapsettings}, function() {
            console.log('>>> map settings saved')
        })
    }); 

    map.on('zoomend', function(e) {
        let mapsettings = {
            longitude: map.getCenter().lng, 
            latitude: map.getCenter().lat,
            zoom: map.getZoom()
        };

        chrome.storage.local.set({mapsettings: mapsettings}, function() {
            console.log('>>> map settings saved')
        })
    }); 

} else {
    document.getElementById('map').innerHTML = `
        <p style="color: red;">There is no internet connection.</p>
    `;
}
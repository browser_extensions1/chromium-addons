

document.querySelector('#saveNewContact').addEventListener('click', function() {
    let newcontact_name = document.querySelector('#newContact-nameInput').value; 
    let newcontact_phone = document.querySelector('#newContact-phoneInput').value; 
    let newcontact_email = document.querySelector('#newContact-emailInput').value; 
    let newcontact_link = document.querySelector('#newContact-linkInput').value; 
    let newcontact_notes = document.querySelector('#newContact-notesInput').value; 

    if (
        newcontact_name != '' ||
        newcontact_phone != '' ||
        newcontact_email != '' || 
        newcontact_notes != '' 
    ) {
        saveNewContact(newcontact_name, newcontact_phone, newcontact_email, newcontact_link, newcontact_notes);
    }

}); 

function saveNewContact(name, phone, email, link, notes) {
    chrome.storage.local.get(['contacts'], function(result) {
        if (result.contacts) {
            let contactsArray = result.contacts; 
            contactsArray.push(
                {
                    name: name,
                    phone: phone,
                    email: email,
                    link: link,
                    notes: notes 
                }
            ); 
            chrome.storage.local.set({contacts: contactsArray}, function() {
                location.reload();
            })

        } else {
            let contactsArray = [];
            contactsArray.push(
                {
                    name: name,
                    phone: phone,
                    email: email,
                    link: link,
                    notes: notes 
                }
            ); 
            chrome.storage.local.set({contacts: contactsArray}, function() {
                location.reload();
            })
            
        }
    });
}


function renderContacts() {
    chrome.storage.local.get(['contacts'], function(result) {
        if (result.contacts) {
            let contactsArray = result.contacts; 
            let contactsList = '';
            let contactDetailList = '';
            for (let i=0; i<contactsArray.length; i++) {
                contactsList += `
                    <a class="list-group-item list-group-item-action py-1" href="#listItem-${i}">${contactsArray[i].name}</a>
                `;
                contactDetailList += `
                    <div id="listItem-${i}" class="card my-3">
                        <div class="card-header">
                            <input id="contactName-${i}" class="form-control form-control-sm input-header" type="text" value="${contactsArray[i].name}"> 
                            
                        </div>
                        <div class="card-body py-1 ps-1">
                            <ul class="ps-2 py-1 ps-0">
                                <li class=" py-1">
                                    <input id="contactPhone-${i}" class="form-control form-control-sm" type="text" value="${contactsArray[i].phone}" placeholder="Phone...">    
                                </li>
                                <li class=" py-1">
                                    <input id="contactEmail-${i}" class="form-control form-control-sm" type="text" value="${contactsArray[i].email}" placeholder="Email...">    
                                </li>
                                <li class=" py-1">
                                    <input id="contactLink-${i}" class="form-control form-control-sm" type="text" value="${contactsArray[i].link}" placeholder="Link...">    
                                </li>
                                <li class=" py-1">
                                    <textarea name="textarea" id="contactNotes-${i}" rows="2" class="form-control form-control-sm" placeholder="Notes...">${contactsArray[i].notes}</textarea>
                                </li>
                            </ul>
                            <div class="dropdown d-flex justify-content-end">
                                <button name="cancelSaveItemButton" id="cancelSaveItemButton-${i}" class="d-none">Cancel</button>
                                <button name="saveItemButton" id="saveItemButton-${i}" class="d-none">Save</button>
                                <button name="editItemButton" id="editItemButton-${i}" class="btn btn-link btn-sm text-primary">Edit</button>
                                <button id="deleteDropdownButton-${i}"  class="btn btn-link btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Delete
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="deleteDropdownButton-${i}">
                                    <li><button name="deleteItemButton" id="deleteItemButton-${i}" class="dropdown-item py-0 my-0 text-danger btn-sm">Delete contact</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                `;
            };
            document.getElementById('contactsList').innerHTML = contactsList; 
            document.getElementById('contacts-detail-block').innerHTML = contactDetailList; 

            // let deleteItemButtons = document.querySelector('button[name="deleteItemButton"]');
            let deleteItemButtons = document.querySelectorAll('button[name="deleteItemButton"]');
            for (let j=0; j<deleteItemButtons.length; j++) {
                deleteItemButtons[j].addEventListener('click', deleteContact);
            };

            let editItemButtons = document.querySelectorAll('button[name="editItemButton"]');
            for (let m=0; m<editItemButtons.length; m++) {
                editItemButtons[m].addEventListener('click', editContact);  
            }

            let cancelSaveItemButtons = document.querySelectorAll('button[name="cancelSaveItemButton"]');
            for (let q=0; q<cancelSaveItemButtons.length; q++) {
                cancelSaveItemButtons[q].addEventListener('click', function() {
                    location.reload();
                })
            }

            let saveItemButtons = document.querySelectorAll('button[name="saveItemButton"]'); 
            for (let w=0; w<saveItemButtons.length; w++) {
                saveItemButtons[w].addEventListener('click', saveContact)
            };
        }
    });
}; 

renderContacts();

function editContact() {
    let itemId = this.id.split('-')[1];
    document.getElementById(`saveItemButton-${itemId}`).className = "btn btn-outline-primary btn-sm d-block"; 
    document.getElementById(`cancelSaveItemButton-${itemId}`).className = "btn btn-outline-primary btn-sm d-block"; 
    document.getElementById(`editItemButton-${itemId}`).className = "btn btn-link btn-sm text-primary d-none"; 
    document.getElementById(`deleteDropdownButton-${itemId}`).className = "btn btn-link btn-sm dropdown-toggle d-none"; 
    
};

function saveContact() {
    let itemId = this.id.split('-')[1];
    let contact_name = document.querySelector(`#contactName-${itemId}`).value; 
    let contact_phone = document.querySelector(`#contactPhone-${itemId}`).value; 
    let contact_email = document.querySelector(`#contactEmail-${itemId}`).value; 
    let contact_notes = document.querySelector(`#contactNotes-${itemId}`).value; 

    if (
        contact_name != '' ||
        contact_phone != '' ||
        contact_email != '' ||
        contact_notes != ''
    ) {
        chrome.storage.local.get(['contacts'], function(result) {
            let contactsArray = result.contacts;
            for (let i=0; i<contactsArray.length; i++) {
                if (i == itemId) {
                    contactsArray[i] = {
                        name: contact_name,
                        phone: contact_phone,
                        email: contact_email,
                        notes: contact_notes 
                    }
                    break;
                }
            };
            chrome.storage.local.set({contacts: contactsArray}, function() {
                location.reload();
            });
        })
    }
}

function deleteContact() {
    let itemId = this.id.split('-')[1];
    chrome.storage.local.get(['contacts'], function(result) {
        let contactsArray = result.contacts; 
        for (let n=0; n<contactsArray.length; n++) {
            if (n == itemId) {
                contactsArray.splice(n, 1)
                break; 
            }
        }
        chrome.storage.local.set({contacts: contactsArray}, function() {
            location.reload();
        })
    })    
}; 



chrome.runtime.onInstalled.addListener( (install) => {
  chrome.storage.local.get(["tasks"], (result) => {
    if (!result.tasks) {
      let arrayToSave = JSON.stringify([])
      chrome.storage.local.set({"tasks": arrayToSave}, () => {
        console.log('tasks set for storage');
      })
    }
  });


  if (install.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.tabs.create({
      url: "https://vwrr-extensions.click"
    });
    console.log(">>> Add-on installed successfully")
  } else if (install.reason === chrome.runtime.OnInstalledReason.UPDATE) {
    console.log(">>> Add-on updated successfully")
  }

});

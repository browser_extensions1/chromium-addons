
chrome.action.onClicked.addListener( () => {
  chrome.tabs.create(
    {active: true, url: "index.html"},
    (tab) => {
      console.log(">>> deleting browsing data")
    }
  )
});


chrome.commands.onCommand.addListener((command) => {
  console.log(`>>> Command: ${command}`);
  if (command === "quick-delete") {
    chrome.tabs.create(
      {active: true, url: "index.html"},
      (tab) => {
        console.log(">>> deleting browsing data")
      },
    )
  }
});


chrome.runtime.onInstalled.addListener( (install) => {
  if (install.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.tabs.create({
      url: "https://vwrr-extensions.click"
    });
    console.log(">>> Add-on installed")
  } else if (install.reason === chrome.runtime.OnInstalledReason.UPDATE) {
    console.log(">>> Add-on updated")
  }
})

chrome.alarms.onAlarm.addListener( () => {
  chrome.tabs.create({
    url: "notification.html"
  })
})

chrome.runtime.onInstalled.addListener( (install) => {
  
  chrome.storage.local.set({"switchon": "on"}, () => {
    console.log(">>> * ");
    
  });

  if (install.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    // chrome.tabs.create({
    //   url: "https://vwrr-extensions.click"
    // });
    console.log(">>> Add-on installed")
    chrome.alarms.create(
      "rule-of-20",
      {periodInMinutes: 20}
    );
  } else if (install.reason === chrome.runtime.OnInstalledReason.UPDATE) {
    console.log(">>> Add-on updated")
  };

})

chrome.runtime.onInstalled.addListener( (install) => {
  chrome.storage.local.get(["cheatsheets"], (result) => {
    if (!result.cheatsheets) {
      let data = JSON.stringify([
        {name: 'Array methods', content: `var colors = ['white', 'red', 'green', 'black'];
colors.toString(); // >>> 'white,red,green,black'
colors.join(' & '); // >>> 'white & red & green & black'
colors.splice(1, 2); // >>> ['red', 'green']
colors.sort(); // >>> ['black', 'green', 'red', 'white']
colors[2]; // >>> 'green'
colors.length; // >>> 4`, isActive: false}, 
        {name: "String methods", content: `var txt = "there is an orange house";
txt.slice(2,3) // >>> e
txt.length; // >>> 24
txt.indexOf("g"); // >>> 16
txt.concat(" and"); // >>> there is an orange house and
txt.split("is"); // >>> ['there', ' an orange house']
txt[6]; // >>> i
txt.includes("orange"); // >>> true`, isActive: false},
        {name: "Operators", content: `x = y // assignment
x == y // equals
x === y // strict equal
x != y // unequal
x !== y // strict unequal
x < y; x > y // less and greater than
x <= y; x >= y // less or equal, greater or equal
x && y // and
x || y // or`, isActive: false},
      ]);
      chrome.storage.local.set({"cheatsheets": data}, () => {
        console.log('>>> ^');
      })
    }
  })

  if (install.reason === chrome.runtime.OnInstalledReason.INSTALL) {
    chrome.tabs.create({
      url: "https://vwrr-extensions.click"
    });
    console.log(">>> Add-on installed successfully")
  } else if (install.reason === chrome.runtime.OnInstalledReason.UPDATE) {
    console.log(">>> Add-on updated successfully")
  };

})




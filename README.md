### Browser extensions for Chromium based browser

##### This repository contains more than ten browser extensions ready to be manually installed into any web browser based on Chromium. They are also available on Edge Add-ons.
.

### Extensions: 
* Rule-of-20
* Quick-delete
* USA-calendar
* ShortNotes
* Quick-JavaScript
* To Do list
* Contacts
* Random Psamls verse
* Simple reminder
* Youtube video link_get saver
* Simple english dictionary
* Simple calendar
* Simple street view map
* Simple history cleaner


## USA-calendar 
##### A simple calendar for the United States.


### How download the add-on?
1. Click on the download button, the one between the "Find file" and "Clone" buttons.
2. Under "Download this directory," click on "zip."
3. Once downloaded, unzip the files.
4. Use the files inside "extension" directory to install on your browser.

### how to install the add-on manually on my browser?
Open the extensions section of your browser, turn on developer mode, and load the extension. For more details, refer to the official browser documentation on how to manually install an add-on.